"use strict"

const fs = require( "fs" );
const fontifier = require( hop.fontifier );

service pldi2020 () {

   return <html>
     <head>
       <style>
	 h3.title,
	 h2.title,
	 h1.title {
	    text-align: center;
	 }
	 table.title {
	    text-align: center;
	    margin-left: auto;
	    margin-right: auto;
	    width: 90%;
	 }
	 td.author table {
	    text-align: center;
	    margin-left: auto;
	    margin-right: auto;
	 }
	 table.title td {
	    text-align: center;
	 }
	 table.title td.author {
	    width: 50%;
	    text-align: center;
	 }
	 span.button {
	    border: 1px solid #777;
	    padding: 0 2px 0 2px;
	    font-size: 80%;
	 }
	 pre {
	    border: 1px solid #777;
	    background: #f7f7f7;
	    font-size: 90%;
	    font-family: monospace;
	    padding: 4px;
	    margin: 1ex 1em 1ex 1em;
	 }
	 div.simulator {
	    border: 1px solid #777;
	    border-radius: 4px;
	    padding: 8px;
	    margin: 1ex 1em 1ex 1em;
	    font-size: 90%;
	    background: #fffdc3;
	 }
	 span.lisinopril {
	    font-variant: small-caps;
	    font-size: 103%;
	 }
	 .TryTooCloseError, .NoDoseSinceTooLongError {
            background-color: #c00;
	    color: white;
	    border-radius: 2px;
            font-weight: bold;
	    display: inline-block;
         }
         .TryNotInWindowWarning {
            background-color: #fc0;
	    color: white;
	    border-radius: 2px;
            font-weight: bold;
	    display: inline-block;
         }
       </style>
       <script src="hiphop" lang="hopscript"/>
       <script src="./Lisinopril/Lisinopril.hh.js" lang="hiphop"/>
       <script defer>
	 const Red    = "#cc0000";
         const Green  = "#00cc00";
	 const Blue   = "#0000cc";
         const Orange = "#FFC000";
	 const Black  = "#000000";
         const White  = "#FFFFFF";
         const Grey   = "#404040";
	 
	 const hh = require ("hiphop");
         const m = new hh.ReactiveMachine(require("./Lisinopril/Lisinopril.hh.js", "hiphop"),
                                          { sweep: true,
					    debuggerName: "LisinoprilDebug",
					    name: "Lisinopril",
					    dumpNets:false });
       </script>
     </head>
     <body>
	    
       <hr/>
       <h1 class="title">HipHop.js: (A)Synchronous Reactive Web Programming</h1>
       <h3 class="title">PLDI 2020 Artifact</h3>
       <table class="title">
	 <tr>
	   <td class="author">
	     <table>
	       <tr> <td>G. Berry</td></tr>
	       <tr><td>College de France</td></tr>
	       <tr><td>Paris, France</td></tr>
	       <tr><td><tt>Gerard.Berry@college-de-france.fr</tt></td></tr>
	     </table>
	   </td>
	   <td class="author">
	     <table>
	       <tr><td>M. Serrano</td></tr>
	       <tr><td>Inria</td></tr>
	       <tr><td>Sophia Antipolis, France</td></tr>
	       <tr><td><tt>Manuel.Serrano@inria.fr</tt></td></tr>
	     </table>
	   </td>
	 </tr>
       </table>
       <hr/>
       <br/>
       <h2>5.1 Simulating the Lisinopril Medical Prescriptions</h2>
       <p>This is the <span class="lisinopril">Lisinopril</span> simulation demo, as detailed in Sections <b>5.1</b>, <b>5.2</b>, and <b>5.3</b> of the paper. <p/>
       
   The program is controlled by the four active buttons START, Mn, Try,
and Conf pictured in the yellow bar after the framed text below, which
comments them and their coloring scheme and also contains a simulation
sequence. This file could also be used as a direct input to the HipHop
batch simulator, hence the % before comments. We put the text before
the active buttons to be able to run the sequence by seeing its
elements with minimal scrolling. Except for START, to be clicked once
when starting the simulation, the buttons match the Lisinopril program
inputs. Click them in the specified order (START, Mn, Mn, etc.) and
check the results. To restart the simulator afresh, reload the web
page. The simulation explores the normal behavior and all the warning
and error cases. Of course, you can run and check your own simulation
by clicking freely if you wish. The real Lisinopril JavaScript
compiled code is at work. Its full hop/hiphop source is in the
Lisinopril subdirectory.
	
        Here is a possible execution scenario that illustrates the behavior
	of this application.
											     
        <pre>${fs.readFileSync( require.resolve( "./Lisinopril/Lisinopril.in" ) ).toString()}</pre>
	
	To execute the scenario, click on the designated buttons of the simulator in the specified order.
	
	<h2>The Simulator</h2>
	
	<div class="simulator">
          <button onclick = ~{ m.react(); } >
	    START
          </button>
	
	  <button style = ~{`background-color: ${m.TimeColor.nowval}`} 
                  onclick = ~{ m.react("Mn"); } >
    	    Mn
          </button>	  

          <button style = ~{`color: ${m.TryActive.nowval ? (m.TryAlert.nowval ? Red : Green) : Black}`}
		  onclick = ~{ m.react("Try")} >
            Try
          </button>
  	  
	  <button style = ~{`color: ${m.ConfActive.nowval ? (m.ConfAlert.nowval ? Red : Green) : Black}`}  
		  onclick = ~{m.react("Conf")} >
	    Conf
          </button>
	  <br/>
          <div>
	    <react>~{<span>Time= ${(m.Time.nowval)}, </span>}</react>
	    <react>~{<span>Day= ${(m.Day.nowval)}, </span>}</react>
	    <react>~{<span>RecordDose= ${(m.RecordDose.nowval)}</span>}</react>
	  </div>       
	  
    	  ${ [ "DeliverDose", "TryNotInWindowWarning", "RecordDose", "TryTooCloseError", "NoDoseSinceTooLongError" ]
          	.map( (sig, idx, arr) =>
	     	   <div class=${sig}>
		     <react>
		       ~{ m[ ${sig} ].now ? ${sig} : " " }
		     </react>
		   </div> )}
	</div>
	
	
       <h2>The Implementation</h2>
The HipHop.js implementation is installed in the 
<tt>/usr/local/lib/hop/3.3.0/node_modules/hiphop</tt>
directory. It can be more easily browsed on our 
 <a href="https://github.com/manuel-serrano/hiphop">github</a> repository.

The implementation of the <span class="lisinopril">Lisinopril</span> application
is stored in the <a href=${source(require.resolve("./Lisinopril/Lisinopril.hh.js" ))}><tt>/tmp/pldi2020-artifact/Lisinopril.hh.js</tt></a> and <a href=${source(require.resolve("./Lisinopril/Lisinopril.js" ))}><tt>/tmp/pldi2020-artifact/Lisinopril.js</tt></a> files. 
								      
A standalone version of the application can be generated by simply using
the <span class="button">Save As</span> facility of your web browser. This will
save all the files needed to run the application without needing Docker anymore.
								    
       <br/>
       <h2>5.4 Skini: massively interactive music</h2>

       <p>Section 5.4 describes a system for producing interactive music. As
 this system requires external synthesizers we cannot provide a running 
 system here. Instead, we merely include references to some recordings that
 are publicly available. These pieces have been produced by executing their
 corresponding HipHop.js program against an audience simulator that interacts
 with the musical score randomly.</p>
       <em>
	 <p>All these pieces have been composed by 
             <a href="https://soundcloud.com/user-651713160">Bertrand Heidelein</a>.</p></em>

       <ul>
	 <li>
       	   <a href="https://soundcloud.com/user-651713160/grand-loup">Jazz Music</a>
	   The score is a 200 line long HipHop.js program using 160 different signals. It uses 80 music
           samples.
	 </li>
	 <li>
       	   <a href="https://soundcloud.com/user-651713160/opus1-skini-v2-chrom">Classical Music</a>
	   The score is a 500 line long HipHop.js program using about 500 signals. It uses 270 music
           samples.
	 </li>
	 <li>
       	   <a href="https://soundcloud.com/user-651713160/lusine-mai-2019/s-gMdN9">Electro Music</a>
	   The score is a 300 line long HipHop.js program. It uses 100 music
           samples.
	 </li>
       </ul>
     </body>
   </html>
}

service source( path ) {
   return new Promise( function( resolve, reject ) {
      var fontify = fontifier.hiphop;
      
      fs.readFile( path, function( err, buf ) {
	 resolve( <pre class="fontifier-prog">${fontify( buf )}</pre> )
      } );
   } );
}
