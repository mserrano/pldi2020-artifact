"use strict"

service Lisinopril () {

   return <html>
     <head>
       <style>
	 span.lisinopril {
	    font-variant: small-caps;
	    font-size: 103%;
	 }
	 .TryTooCloseError, .NoDoseSinceTooLongError {
            background-color: #c00;
	    color: white;
	    border-radius: 2px;
            font-weight: bold;
         }
         .TryNotInWindowWarning {
            background-color: #fc0;
	    color: white;
	    border-radius: 2px;
            font-weight: bold;
         }
       </style>
       <script src="hiphop" lang="hopscript"/>
       <script src="./Lisinopril.hh.js" lang="hiphop"/>
       <script defer>
	 const Red    = "#FF0000";
         const Green  = "#00FF00";
	 const Blue   = "#0000FF";
         const Orange = "#FFC000";
	 const Black  = "#000000";
         const White  = "#FFFFFF";
         const Grey   = "#404040";
	 
	 const hh = require ("hiphop");
         const m = new hh.ReactiveMachine(require("./Lisinopril.hh.js", "hiphop"),
                                          { sweep: true,
					    debuggerName: "LisinoprilDebug",
					    name: "Lisinopril",
					    dumpNets:false });
       </script>
     </head>
     <body>
	
        <button onclick = ~{ m.react(); } >
	   START
        </button>
	
	<button style = ~{`background-color: ${m.TimeColor.nowval}`} 
                onclick = ~{ m.react("Mn"); } >
    	   Mn
        </button>	  

        <button  style = ~{`color: ${m.TryActive.nowval ? (m.TryAlert.nowval ? Red : Green) : Black}`}
                 onclick = ~{ m.react("Try")} >
           Try
        </button>
  	  
	<button	style = ~{`color: ${m.ConfActive.nowval ? (m.ConfAlert.nowval ? Red : Green) : Black}`}  
                onclick = ~{m.react("Conf")} >
	   Conf
        </button>
	<br/>
        <div>
	  <react>~{<span>Time= ${(m.Time.nowval)}, </span>}</react>
	  <react>~{<span>Day= ${(m.Day.nowval)}, </span>}</react>
	  <react>~{<span>RecordDose= ${(m.RecordDose.nowval)}</span>}</react>
	</div>       

    ${ [ "DeliverDose", "TryNotInWindowWarning", "RecordDose", "TryTooCloseError", "NoDoseSinceTooLongError" ]
          .map( (sig, idx, arr) =>
	     <div>
	       <react>~{ m[ ${sig} ].now ? ${sig} : " " }</react>
	     </div> )}
	
     </body>
   </html>
}
