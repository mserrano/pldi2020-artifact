HipHop.js PLDI 2020 artifact - UPDATE on 09/03/2019
===================================================

Getting Started Guide
---------------------

Our artifact consists of:

  - The current HipHop.js implementation, which you can either download 
    fully constructed in the docker file available from the PLDI artifact
    site (see Part 1 below) or entirely rebuild from the public
    Hop/HipHop sources files (see Part 3) if you find the 1.3 GB docker
    file too big or if you want to do it yourself. Once the docker image is
    downloaded or built, you can validate it by running the standard
    HipHop implementation test suite (see Part 4).

  - The implementation and step-by-step simulation of our Lisinopril
    medical prescription application described in the paper (Part 2 below).
    The main Lisinopril HipHop reactive module is exacly the one in the
    paper. The complete source files can be accessed within the docker image
    or dowloaded from the web.

    We provide you with a commented simulation sequence you can execute
    interactively in a dedicated web built by HipHop. This sequence exercises
    diverse execution cases that validate the paper's claim on the program's
    behavior. It can also be run in batch mode.

    Of course, you can run your own sequences at will by clicking
    differently.


PART 1: downloading the image
-----------------------------

Download the pre-built image available at the following address:

 https://drive.google.com/file/d/17NbWA1_8QZEF22KImT5WtgpBvUbfw8N1/view?usp=sharing
 
Unzip the image:

```
gzip -d hiphop-pldi2020.tar.gz
```

Load the image into docker:

```
docker load < hiphop-pldi2020.tar
```

Part 2: Running the Lisinopril step-by-step simulation
------------------------------------------------------

The Lisinopril medical prescription application is available as a standard
web service you can easily exercise. The web server part will be executed
within the Docker image and the client part will be interactively executed
by your preferred local web browser.

 1. once docker has loaded the image, run the Lisinopril server as follows:

```
docker run -p 8080:8080 --entrypoint=/usr/local/bin/hop hiphop-pldi2020 -p 8080 -v --no-zeroconf /tmp/pldi2020-artifact/pldi2020.js
```

 2. This builds a web page that you can load and execute in your browser;
    only the generated code is loaded, no extra runtime is necessary.
    Load this page at the following address:

 http://localhost:8080/hop/pldi2020

 You will see a framed text that contains a simulation input
 sequence. This sequence could be executed in batch mode, but we
 propose that you execute it first in interactive mode. For this,
 scroll to position the buttons enclosed the frame so that you see the
 simulation sequence lines above them. Click these buttons one by one
 in the order specified aboove them and check at each step that the
 results listed below the buttons match the corresponding comments in
 the sequence above.

 To reset the simulation, just reload the URL in your
 browser. Of course, you can also execute your own test by clicking
 the buttons in any different order.

 On demand, we can also provide you with a way to run the provided
 Lisinopril.in file or your own files in batch  mode, which is of
 course necessary or regression testing when you modify an application.

To see the Hop/HipHop source files of the Lisinopril application, start a docker
shell as follows :

```
docker run -t -i --entrypoint=/bin/bash hiphop-pldi2020
cd /tmp/pldi2020-artifact/Lisinopril/
```

The source file Lisinopril.js (Hop) and Lisinopril.hh.js (HipHop) are
in the directory you reached within docker. The test file
Lisinopril.in is also available there, and you can also run it in
batch mode if you have downloaded or rebuilt the new docker image made
available on March 9th. For this, type the following lines in your
current docker shell:

```
cd /tmp/pldi2020-artifact/Lisinopril/
hop -g --no-server ./test-lisinopril.js < Lisinopril.in
```

Part 3 : Build the Hiphop docker image locally.
-----------------------------------------------

For that, simply proceed by running the following commands in a Unix terminal:

```
cat > Dockerfile <<EOF
FROM debian:buster

RUN apt-get update && apt-get install -y \
   bash \
   make \
   wget \
   git \
   gcc \
   autoconf \
   automake \
   curl \
   pkg-config \
   libtool \
   libgmp-dev libgmp3-dev libgmp10 \
   libssl1.1 libssl-dev \
   libsqlite3-0 libsqlite3-dev \
   libasound2 libasound2-dev \
   libpulse0 libpulse-dev \
   libflac8 libflac-dev \
   libmpg123-0 libmpg123-dev \
   libavahi-core7 libavahi-core-dev libavahi-common-dev \
   libavahi-common3 \
   libavahi-client3 libavahi-client-dev

WORKDIR /tmp

RUN wget ftp://ftp-sop.inria.fr/indes/fp/Bigloo/bigloo4.3g.tar.gz \
   && tar -xzf bigloo*.tar.gz && cd bigloo*/ \
   && ./configure && make -j && make install

RUN git clone https://github.com/manuel-serrano/hop \
   && cd hop \
   && git checkout 0e0f8b0d9e1c732b4cd77e0677a138dcbe5accba \
   && ./configure --license=academic --disable-doc \
   && make -j && make install

RUN git clone https://github.com/manuel-serrano/hiphop.git \
   && (cd hiphop; git checkout 402a8697d1606f42a035570b8cf23d0ce01bb63b) \
   && mv hiphop /usr/local/lib/hop/3.3.0/node_modules \
   && chmod a+rx -R /usr/local/lib/hop/3.3.0/node_modules/hiphop

RUN useradd --no-log-init -d /home/hop -s /bin/bash hop

RUN mkdir -p /home/hop/.config/hop && \
   echo "(add-user! \"anonymous\" :directories '* :services '*)" \
     > /home/hop/.config/hop/hoprc.hop && \
   chown -R hop /home/hop

RUN git clone https://gitlab.inria.fr/mserrano/pldi2020-artifact \
   && chmod a+rwx -R /tmp/pldi2020-artifact

USER hop
ENV HOME /home/hop

STOPSIGNAL SIGINT

ENTRYPOINT ["/bin/bash"]
ENTRYPOINT ["/usr/local/bin/hop"]
ENTRYPOINT ["/tmp/pldi2020-artifact/pldi2020.sh"]
EOF
docker build -t hiphop-pldi2020 .
```

Then proceed by Phase 2 above to run the Lisinopril simulation and Part 4
for running the HipHop test


Part 4 : checking the HipHop construction
-----------------------------------------

Once the image is created, the basic testing of HipHop.js can be executed
with:

```
docker run hiphop-pldi2020 /tmp/pldi2020-artifact/pldi2020.sh
```

This will execute the HipHop.js standard tests within the Docker image. The
result of this execution is expected to be as follows:

 *** 274 success   0 failure   11 missing   1 blacklist ***

To list and see the tests, enter docker as follows:

```
docker run -t -i --entrypoint=/bin/bash hiphop-pldi2020
cd /usr/local/lib/hop/3.3.0/node_modules/hiphop/tests
```

Note that these tests can also be viewed at address:

 https://github.com/manuel-serrano/hiphop/tree/master/tests

