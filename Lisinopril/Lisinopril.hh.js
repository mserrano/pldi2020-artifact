"use hiphop"
"use hopscript"

var hh = require("hiphop");

//===============================================
// Lisinopril prescription in HipHop, by G. Berry
//===============================================

const Orange = "#FFC000";
const Green  = "#00FF00";

/*
Time constants for actual exexution - outcomment for simulation

const MnPerDay = 24*60,
      OpenDoseWindowTime = 20*60,
      CloseDoseWindowTime = 23*60,
      TryDelay = 6*60,
      MinDoseInterval  = 10*60,
      MaxDoseInterval = 34*60;
      ConfDelay = 5;
      
*/

// Time constants for simulation - outcomment for execution

const MnPerDay = 8,
      OpenDoseWindowTime = 4,
      CloseDoseWindowTime = 7,
      TryDelay = 6,
      MinDoseInterval = 4, 
      MaxDoseInterval = 14,
      ConfDelay = 3;

function IsInDoseWindow (t) {
    return (t >= OpenDoseWindowTime && t < CloseDoseWindowTime);
}

function GetTimeColor (t) {
    return (IsInDoseWindow(t) ? Green : Orange);
}

//-----------------------------------------------------------------------
// Clock module : computes and broasdcasts hour and day
//                assuming the program start at mignight.
//                (should read system clock).
//                Also emits TimeColor as Green or Orange according to
//                specified prescription window.
//-----------------------------------------------------------------------

hiphop module Clock (in  Mn,            
                     out Time,              // int (should become {int; int;}
		     out Day,               // int
		     out InDoseWindow,      // bool
		     out TimeColor = Orange) {  
   
   fork {
      emit Time(0);
      emit TimeColor (GetTimeColor(Time.nowval));
      every (Mn.now) {
	 emit Time ((Time.preval+1) % MnPerDay);
         emit InDoseWindow (IsInDoseWindow(Time.nowval));
         emit TimeColor(GetTimeColor(Time.nowval));
      }
   } par {
      // compute Day
      emit Day(1);
      every (Time.now && Time.nowval === 0) {
	 emit Day (Day.preval+1);
      }
   }	
		    }

//--------------
// Button module
//--------------

hiphop module Button (var d, in Tick, in  B, out Active, out Alert) {
   emit Active (true); emit Alert (false);
   abort (B.now) {
      await count(d, Tick.now);
      do { emit Alert (true) } every (Tick.now);
   };
   emit Alert (false); emit Active (false);
}


//-----------------------------
// The Lisinopril module proper
//-----------------------------

hiphop module Lisinopril (in  Mn,             
			  out Time,
			  out Day,
			  out TimeColor,
			  in  InDoseWindow, // bool
			  in  Try,
			  out TryActive= false,
			  out TryAlert,
			  out TryNotInWindowWarning,
			  out TryTooCloseError,
			  in  Conf,
			  out ConfActive = false,
			  out ConfAlert,
			  out DeliverDose,
			  out RecordDose,
			  out NoDoseSinceTooLongError) {
   signal InDoseWindow;
   fork {
      run Clock (...);
   } par {
      loop {
         DoseOK : fork{
	    run Button(d =TryDelay, Tick as Mn, B as Try,
		       Active as TryActive, Alert as TryAlert);
	    // Try received, deliver but warn if out of dose window
	    emit DeliverDose ();
	    if (! InDoseWindow.nowval) {
	       emit TryNotInWindowWarning ();
	    }
	    // phase 2 : wait for confirmation, keep alerting if late
	    run Button(d =ConfDelay, Tick as Mn, B as Conf,
		       Active as ConfActive, Alert as ConfAlert);
	    // confirmation received
	    emit RecordDose (Time.nowval);
	    break DoseOK;
	 } par {
	    // in phases 2-3, error if too long wait since last done
	    await count(MaxDoseInterval-MinDoseInterval, Mn.now);
	     abort (Conf.now) { sustain NoDoseSinceTooLongError (); }
	 }
	 // phase 3 : wait for min delay to allow Try again
	 //           error if Try pressed although normally inactive
	 abort count(MinDoseInterval, Mn.now) {
	    every (Try.now) {
	       emit TryTooCloseError ();
	    }
	 }
      }
   } 
}

module.exports = Lisinopril;


       
