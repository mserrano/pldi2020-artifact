FROM debian:buster

RUN apt-get update && apt-get install -y \
    bash \
    make \
    wget \
    git \
    gcc \
    autoconf \
    automake \
    curl \
    pkg-config \
    libtool \
    libgmp-dev libgmp3-dev libgmp10 \
    libssl1.1 libssl-dev \
    libsqlite3-0 libsqlite3-dev \
    libasound2 libasound2-dev \
    libpulse0 libpulse-dev \
    libflac8 libflac-dev \
    libmpg123-0 libmpg123-dev \
    libavahi-core7 libavahi-core-dev libavahi-common-dev \
    libavahi-common3 \
    libavahi-client3 libavahi-client-dev

WORKDIR /tmp

RUN wget ftp://ftp-sop.inria.fr/indes/fp/Bigloo/bigloo4.3g.tar.gz \
    && tar -xzf bigloo*.tar.gz && cd bigloo*/ \
    && ./configure && make -j && make install

RUN git clone https://github.com/manuel-serrano/hop \
    && cd hop \
    && git checkout 0e0f8b0d9e1c732b4cd77e0677a138dcbe5accba \
    && ./configure --license=academic --disable-doc \
    && make -j && make install

RUN git clone https://github.com/manuel-serrano/hiphop.git \
    && (cd hiphop; git checkout 402a8697d1606f42a035570b8cf23d0ce01bb63b) \
    && mv hiphop /usr/local/lib/hop/3.3.0/node_modules \
    && chmod a+rx -R /usr/local/lib/hop/3.3.0/node_modules/hiphop

RUN useradd --no-log-init -d /home/hop -s /bin/bash hop

RUN mkdir -p /home/hop/.config/hop && \
    echo "(add-user! \"anonymous\" :directories '* :services '*)" \
      > /home/hop/.config/hop/hoprc.hop && \
    chown -R hop /home/hop

RUN git clone https://gitlab.inria.fr/mserrano/pldi2020-artifact \
    && chmod a+rwx -R /tmp/pldi2020-artifact

USER hop
ENV HOME /home/hop

STOPSIGNAL SIGINT

ENTRYPOINT ["/bin/bash"]
ENTRYPOINT ["/usr/local/bin/hop"]
ENTRYPOINT ["/tmp/pldi2020-artifact/pldi2020.sh"]
