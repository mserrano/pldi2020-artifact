"use hopscript"
"use strict"

const hh = require( "hiphop" );
let Lisinopril = require( "./Lisinopril.hh.js", "hiphop" );

try {
   new hh.ReactiveMachine( Lisinopril, "Lisinopril" ).batch();
} catch( e ) {
   console.log( e.message );
   process.exit( 1 );
}
